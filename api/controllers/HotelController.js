const renamer = require('fs'),
  fs= require('fs-extra'),
  tempPath = require('path').resolve(sails.config.appPath, '.tmp/public/images/hotels/'),
  assetPath = require('path').resolve(sails.config.appPath, 'assets/images/hotels/');

var googleMapsClient = require('@google/maps').createClient({
  key: 'AIzaSyB8-GllalpxSUubV2qgh0rhba-wsKE5t3c'
});

module.exports = {
  createNewCompany:function (req,res) {
    var params = req.allParams();
    // var city = {city:params.city};
    var city = {city:"ChIJq_0qLneG1ZERDKDI-2271dw"};
    var company = {name:params.name,
      description:params.description,
      latitude:params.latitude,
      longitude:params.longitude};
    CityHotel.findOrCreate(city).populate('companies').exec(function(error, city){
      if(error) return res.serverError(error);
      Hotel.create(company).exec(function (err,company) {
        if(err) return res.serverError(err);
        city.companies.add(company);
        city.save(function (err) {
          if(err) return res.serverError(err);
          req.file('images')
            .upload({
              dirname: tempPath+'/'+company.id
            },function (err, uploadedFiles) {
              if (err) return res.serverError(err);
              for(var i=0;i<uploadedFiles.length;i++){
                var renamed=company.id+'/'+ i+'.png';
                try {
                  fs.copySync(uploadedFiles[i].fd, assetPath+'/'+renamed);
                  renamer.rename(uploadedFiles[i].fd,tempPath+'/'+renamed);
                } catch (err) {
                  console.error(err)
                }
              }
              company.totalImages=uploadedFiles.length;
              company.save();
              return res.json(company);
            });
        });
      });
    });
  },

  getNearCompanies:function(req,res){
    var latlng = req.param("lat") +","+req.param("long");
    var radio = req.param("radio") * 1000;
    googleMapsClient.reverseGeocode({
      latlng : latlng
    }, function(err, response) {
      if (!err) {
        if(response.json.results.length<3){return res.json(Array());}
        CityHotel.findOne({city:"ChIJq_0qLneG1ZERDKDI-2271dw"})
          .populate('companies')
          .exec(function (err,city) {
            if (err) return res.serverError(err);
            if (!city) return res.json(Array());
            var totalCompanies=0;
            var companies=Array();
            city.companies.forEach(function(comp,index,arr){
              googleMapsClient.distanceMatrix({origins:latlng,destinations:comp.latitude+","+comp.longitude},function (err,distance) {
                totalCompanies++;
                var distance = distance.json.rows[0].elements[0].distance;
                if(distance.value<=radio) {
                  comp.distanceNumber = distance.value;
                  comp.distanceText = distance.text;
                  companies.push(comp);
                }
                if(totalCompanies==arr.length){
                  return res.json(companies.sort(function (a,b) {
                    return a.distanceNumber - b.distanceNumber;
                  }));
                }
              });
            });
          });
      }
    });
  },
};

