/**
 * Hotel.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  autoCreatedAt: false,
  autoUpdatedAt: false,
  attributes: {
    name: {
      type:'string'
    },
    description: {
      type:'string'
    },
    rating: {
      type: 'float',
      defaultsTo: 5
    },
    totalUsers: {
      type: 'integer',
      defaultsTo: 1
    },
    totalScore: {
      type: 'integer',
      defaultsTo: 5
    },
    latitude: {
      type: 'float'
    },
    longitude: {
      type: 'float'
    },
    place: {
      model: 'cityhotel'
    }
  }
};

